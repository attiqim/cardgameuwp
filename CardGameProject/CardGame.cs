﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UniversalCardGame
{
    struct GameScore
    {
        //private int _playerScore;
        //private int _houseScore;
        //because no  business logic with the score use,  use automatic properties
        public int PlayerScore
        {
            get;set;
        }
        public int HouseScore
        {
            get;set;
        }

        public GameScore(int playerScore, int houseScore)
        {
            this.PlayerScore = playerScore;
            this.HouseScore = houseScore;
        }
    }
    class CardGame
    {
        //Define Properties for these field variables
        private CardDeck _deck;

        private GameScore _score;

        private Card _playerCard;

        private Card _houseCard;
        //Define  constructor
        public CardGame()
        {
            //initialize field variables
            _deck = new CardDeck();
            _score = new GameScore();
            _playerCard = null;
            _houseCard = null;
        }

        public void Play()
        {
            sbyte roundResullt = PlayRound();
        }
        /// <summary>
        /// plays a round in the game
        /// </summary>
        /// <returns>
        /// +1: Player won the round
        /// -1: The house won the round
        /// 0: the round was a draw
        /// </returns>
        public sbyte PlayRound()
        {
            string exchangeInput;
            //determine the ranks of the player and house cards
            byte playerCardRank = DetermineCardRank(_playerCard);
            byte houseCardRank = DetermineCardRank(_houseCard);
            //determine who won the round, the player or the house
            if (playerCardRank > houseCardRank)
            {
                //player won the round
                return 1;
            }
            else if (houseCardRank > playerCardRank)
            {
                //house won the round
                return -1;
            }
            else
            {
                //round ended in draw
                return 0;
            }
          
        }
        /// <summary>
        /// Determines the rank of a card such that Ace is the highest rank
        /// </summary>
        /// <param name="card"></param>
        /// <returns> 
        /// 14: if the value is an Ace
        /// value of the card for any other card
        /// </returns>
        public byte DetermineCardRank(Card card)
        {
            //check for Ace to ensure it has the highest rank
            if (card.Value == 1)
            {
                //This is the Ace Card
                return 14;
            }
            else
            {
                //This is a regular card
                return card.Value;
            }
        }
    }
}
