﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UniversalCardGame
{
    class CardDeck
    {
        private List<Card> _cardList;

        public CardDeck()
        {
            _cardList = new List<Card>();
        }
        /// <summary>
        /// Number of cards in the deck
        /// </summary>
        public int CardCount
        {
            get
            {
                return _cardList.Count;
            }
        }
    }
}
